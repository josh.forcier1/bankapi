import express from "express";
import { Account, Client } from "./entities";
import { InsufficientFundsError, MissingResourceError } from "./errors";
import BankService from "./services/bank-service";
import { BankServiceImpl } from "./services/bank-service-impl";

const app = express();
app.use(express.json());

const bankService:BankService = new BankServiceImpl();

app.post("/clients", async (req,res) =>{

    let aClient:Client = req.body;
    aClient = await bankService.registerClient(aClient);
    res.status(201);
    res.send(aClient)  

});

app.get("/clients", async(req,res) =>{
    try {
        const clients:Client[] = await bankService.retrieveAllClients();
        res.status(200);
        res.send(clients);
    } 
    catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/clients/:id", async(req,res) =>{
    try {
        const clientId:number = Number(req.params.id);
        const aClient:Client = await bankService.retrieveClientById(clientId);
        res.send(aClient);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/clients/:id", async(req,res) =>{
    try {
        let aClient:Client = req.body;
        const clientId:number = Number(req.params.id);
        aClient.clientId = clientId;
        aClient = await bankService.updateClient(aClient);
        res.send(aClient);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.delete("/clients/:id", async(req,res) =>{
    try {
        const clientId:number = Number(req.params.id);
        const bool:boolean = await bankService.deleteClientbyId(clientId);
        res.status(205);
        res.send(bool);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.post("/clients/:id/accounts", async(req,res) =>{
    try {
        let account:Account = req.body;
        const clientId:number = Number(req.params.id);
        account = await bankService.createAccount(clientId,account)
        res.status(201);
        res.send(account);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/clients/:id/accounts", async(req,res) =>{
    try {
        const clientId:number = Number(req.params.id);
        const accounts:Account[] = await bankService.accessAllAccountsOfClient(clientId);
        res.send(accounts);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/accounts/:id", async(req,res) =>{
    try {
        const accountId:number = Number(req.params.id);
        const account:Account = await bankService.accessAccountById(accountId);
        res.send(account);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.put("/accounts/:id", async(req,res) =>{
    try {
        let account:Account = req.body;
        const accountId:number = Number(req.params.id);
        account.accountId = accountId;
        account = await bankService.updateAccount(account);
        res.send(account);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.delete("/accounts/:id", async(req,res) =>{
    try {
        const accountId:number = Number(req.params.id);
        const bool:boolean = await bankService.deleteAccountById(accountId)
        res.send(bool);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.patch("/accounts/:id/deposit", async(req,res) =>{
    try {
        const accountId:number = Number(req.params.id);
        const body = req.body;
        const howMuch:number = body.amount;
        const result = await bankService.depositInAccount(accountId,howMuch);
        res.send(result)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

app.patch("/accounts/:id/withdraw", async(req,res) =>{
    try {
        const accountId:number = Number(req.params.id);
        const body = req.body;
        const howMuch:number = body.amount;
        const result = await bankService.withdrawFromAccount(accountId,howMuch);
        res.send(result)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
        }
        else if (error instanceof InsufficientFundsError){
            res.status(422);
        }
        res.send(error);
    }
})

app.listen(5432,()=>{console.log("Application Started")});

