import { Client, Account } from "../entities";
import { BankDAO } from "./bank-dao";
import { client } from "../connection";
import { MissingResourceError } from "../errors";


export class BankDaoPostgress implements BankDAO{
    async createClient(aClient: Client): Promise<Client> {
        const sql:string = 'insert into client (first_name,last_name) values ($1,$2) returning client_id'
        const values = [aClient.firstName,aClient.lastName];
        const result  = await client.query(sql,values);
        aClient.clientId = result.rows[0].client_id;
        return aClient;
    }

    async getAllClients(): Promise<Client[]> {
        const sql:string = 'select * from client';
        const result = await client.query(sql);
        const clients:Client[] = [];
        for (const row of result.rows){
            const aClient:Client = new Client(row.client_id,row.first_name,row.last_name);
            clients.push(aClient);
        }
        return clients;
    }

    async getClientById(clientId: number): Promise<Client> {
        const sql:string = 'select * from client where client_id = $1';
        const values = [clientId];
        const result = await client.query(sql,values);
        if (result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${clientId} does not exist`);
        }
        const row = result.rows[0];
        const aClient:Client = new Client(row.client_id,row.first_name,row.last_name);
        return aClient;
    }

    async updateClient(aClient:Client): Promise<Client> {
        const sql:string = 'update client set first_name = $1, last_name = $2 where client_id = $3';
        const values = [aClient.firstName,aClient.lastName,aClient.clientId];
        const result = await client.query(sql,values);
        if (result.rowCount ===0){
            throw new MissingResourceError(`The client with id ${aClient.clientId} does not exist`);
        }
        return aClient;
    }

    async deleteClientById(clientId: number): Promise<boolean> {
        
            const sql:string = 'delete from client where client_id = $1';
            const values = [clientId];
            const result = await client.query(sql,values);
            if (result.rowCount === 0){
                throw new MissingResourceError(`The client with id ${clientId} does not exist`);
            }
            return true;
        
    }

    async createAccount(clientId: number, account: Account): Promise<Account> {
        try {
            const sql:string = 'insert into account (balance,account_type,c_id) values ($1,$2,$3) returning account_id'
            const values = [account.balance,account.accountType,clientId];
            const result  = await client.query(sql,values);
            account.accountId = result.rows[0].account_id;
            return account;
        } catch (error) {
            throw new MissingResourceError(`Cannot create account, the client with id ${clientId} does not exist`)
        }

    }

    async getAllAccountsOfClient(clientId: number): Promise<Account[]> {
        const sql:string = 'select * from account where c_id = $1';
        const values = [clientId];
        const result = await client.query(sql,values);
        const accounts:Account[] = [];
        for (const row of result.rows){
            const account:Account = new Account(row.account_id,row.balance,row.account_type);
            accounts.push(account);
        }
        if (result.rowCount === 0){
            throw new MissingResourceError(`Either the client with id ${clientId} does not exist or they have no accounts`);
        }
        return accounts;
    }

    async getAccountById(accountId: number): Promise<Account> {
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId];
        const result = await client.query(sql,values);
        const row = result.rows[0];
        if (result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        const account:Account = new Account(row.account_id,row.balance,row.account_type);
        return account;
    }

    async updateAccount(account: Account): Promise<Account> {
        const sql:string = 'update account set balance = $1, account_type = $2 where account_id = $3';
        const values = [account.balance,account.accountType,account.accountId];
        const result = await client.query(sql,values);
        if (result.rowCount ===0){
            throw new MissingResourceError(`The account with id ${account.accountId} does not exist`);
        }
        return account;
    }

    async deleteAccountById(accountId: number): Promise<boolean> {
        const sql:string = 'delete from account where account_id = $1';
        const values = [accountId];
        const result = await client.query(sql,values);
        if (result.rowCount ===0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        return true;
    }
}