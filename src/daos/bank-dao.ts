import { Account, Client } from "../entities";

export interface BankDAO{

    createClient(client:Client):Promise<Client>;

    getAllClients():Promise<Client[]>;

    getClientById(clientId:Number):Promise<Client>;

    updateClient(client:Client):Promise<Client>;

    deleteClientById(clientId:Number):Promise<boolean>;

    createAccount(clientId:Number, account:Account):Promise<Account>;

    getAllAccountsOfClient(cliendId:Number):Promise<Account[]>

    //GET /accounts?amountLessThan=2000&amountGreaterThan400 => get all accounts for between 400 and 200

    getAccountById(accountId:Number):Promise<Account>;

    updateAccount(account:Account):Promise<Account>;

    deleteAccountById(accountId:Number):Promise<boolean>;

    // depositIntoAccount(accountId:Number, amount:Number):Promise<Account>;

    // withdrawFromAccount(accountId:Number, amount:Number):Promise<Account>;

}


