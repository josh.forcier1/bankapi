import { BankDAO } from "../daos/bank-dao";
import { BankDaoPostgress } from "../daos/bank-dao-postgres";
import { Client, Account } from "../entities";
import { InsufficientFundsError } from "../errors";
import BankService from "./bank-service";


export class BankServiceImpl implements BankService{

    bankDAO:BankDAO = new BankDaoPostgress();


    registerClient(aClient: Client): Promise<Client> {
        return this.bankDAO.createClient(aClient);
    }

    retrieveAllClients(): Promise<Client[]> {
        return this.bankDAO.getAllClients();
    }

    retrieveClientById(clientId: number): Promise<Client> {
        return this.bankDAO.getClientById(clientId);
    }



    //Take closer look at this to be sure it works
    async updateClient(aClient:Client): Promise<Client> {
        aClient = await this.bankDAO.updateClient(aClient);
        return aClient;
    }

    deleteClientbyId(clientId: number): Promise<boolean> {
        return this.bankDAO.deleteClientById(clientId);
    }

    createAccount(clientId: number, account: Account): Promise<Account> {
        return this.bankDAO.createAccount(clientId, account);
    }
    
    accessAllAccountsOfClient(clientId: number): Promise<Account[]> {
        return this.bankDAO.getAllAccountsOfClient(clientId);
    }

    accessAccountById(accountId: number): Promise<Account> {
        return this.bankDAO.getAccountById(accountId);
    }

    async updateAccount(account: Account): Promise<Account> {
        account = await this.bankDAO.updateAccount(account);
        return this.bankDAO.updateAccount(account);
    }

    deleteAccountById(accountId: number): Promise<boolean> {
        return this.bankDAO.deleteAccountById(accountId);
    }

    async depositInAccount(accountId: number, amount: number): Promise<Account> {
        let account:Account = await this.bankDAO.getAccountById(accountId);
        account.balance = account.balance + amount;
        account = await this.bankDAO.updateAccount(account);
        return account;
    }

    async withdrawFromAccount(accountId: number, amount: number): Promise<Account> {
        let account:Account = await this.bankDAO.getAccountById(accountId);
        if (account.balance<amount){
            throw new InsufficientFundsError(`The account with id ${accountId} does not exist`);
        }
        account.balance = account.balance - amount;
        account = await this.bankDAO.updateAccount(account);
        return account;
    }

}