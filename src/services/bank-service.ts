import { Account, Client } from "../entities";

export default interface BankService{

    registerClient(aClient:Client):Promise<Client>;

    retrieveAllClients():Promise<Client[]>;

    retrieveClientById(clientId: number):Promise<Client>;

    updateClient(aClient: Client):Promise<Client>

    deleteClientbyId(clientId: number):Promise<boolean>;
    
    createAccount(clientId:number, account:Account):Promise<Account>;

    accessAllAccountsOfClient(clientId:number):Promise<Account[]>;

    accessAccountById(accountId:number):Promise<Account>;

    updateAccount(account:Account):Promise<Account>;

    deleteAccountById(accountId:number):Promise<boolean>;

    depositInAccount(accountId:number,amount:number):Promise<Account>;

    withdrawFromAccount(accountId:number,amount:number):Promise<Account>;




}