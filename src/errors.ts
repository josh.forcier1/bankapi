

export class MissingResourceError{

    message:string;
    description:string = "This error means a resource could not be found"
    constructor(message:string){
        this.message = message;
    }
}

export class InsufficientFundsError{
    message:string;
    description:string = "This error means the account didn't have enough funds";
    constructor(message:string){
        this.message = message;
    }
}