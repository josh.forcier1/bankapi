import {Client} from 'pg';

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:'bankapidb',
    port:5432,
    host:'34.95.61.179'
})
client.connect();