import { client } from "../src/connection";
import { BankDAO } from "../src/daos/bank-dao";
import { BankDaoPostgress } from "../src/daos/bank-dao-postgres";
import { Account, Client } from "../src/entities";

const bankDao:BankDAO = new BankDaoPostgress;

const testClient:Client = new Client(0,'Josh','Forcier');
const testAccount:Account = new Account(0,500,'Savings');

test("Create a Client",async ()=>{
    const result:Client = await bankDao.createClient(testClient);
    expect(result.clientId).not.toBe(0);
});

test("Get all clients", async ()=>{
    let client1:Client = new Client(0, 'Tim', 'Willard');
    client1 = await bankDao.createClient(client1)
    let client2 = new Client(0, 'John', 'Green');
    client2 = await bankDao.createClient(client2)
    let client3 = new Client(0, 'Stacy', 'Bradford');
    client3 = await bankDao.createClient(client3)

    const clients:Client[] = await bankDao.getAllClients();
    expect(clients.length).toBeGreaterThanOrEqual(3);
});

test("Get client by Id", async () =>{
    let aClient:Client = new Client(0, 'Jordan', 'Butterson');
    aClient = await bankDao.createClient(aClient);
    let retrievedClient:Client = await bankDao.getClientById(aClient.clientId);
    expect(retrievedClient.lastName).toBe(aClient.lastName);
});

test("Update client", async () =>{
    let aClient:Client = new Client(0, 'Alice', 'Rutherford');
    aClient = await bankDao.createClient(aClient);

    aClient.lastName = 'Brown';
    aClient = await bankDao.updateClient(aClient);
    expect(aClient.lastName).toBe('Brown');
});

test("Delete client by id", async () => {
    let aClient:Client = new Client(0,'Anne', 'Barber');
    aClient = await bankDao.createClient(aClient);
    const result:boolean = await bankDao.deleteClientById(aClient.clientId);
    expect(result).toBeTruthy();
})

test("Create an account",async ()=>{
    let aClient:Client = new Client(0, 'Mike', 'Wilmar');
    aClient = await bankDao.createClient(aClient);
    const result:Account = await bankDao.createAccount(aClient.clientId, testAccount);
    expect(result.accountId).not.toBe(0);
});

test("Get all accounts", async ()=>{
    let aClient:Client = new Client(0, 'Bill', 'Hill');
    aClient = await bankDao.createClient(aClient);
    let account1:Account = new Account(0,500,'Checking');
    account1 = await bankDao.createAccount(aClient.clientId, account1)
    let account2:Account = new Account(0,10,'Savings');
    account2 = await bankDao.createAccount(aClient.clientId, account2)
    let account3:Account = new Account(0,40000,'Retirement');
    account3 = await bankDao.createAccount(aClient.clientId, account3)

    
    const accounts:Account[] = await bankDao.getAllAccountsOfClient(aClient.clientId);
    expect(accounts.length).toBeGreaterThanOrEqual(3);
});

test("Get account by Id", async () =>{
    let aClient:Client = new Client(0, 'Mary', 'Lamb');
    aClient = await bankDao.createClient(aClient);
    let account:Account = new Account(0, 340, 'Checking');
    account = await bankDao.createAccount(aClient.clientId,account);
    let retrievedAccount:Account = await bankDao.getAccountById(account.accountId);
    expect(retrievedAccount.balance).toBe(account.balance);
});

test("Update account", async () =>{
    let aClient:Client = new Client(0, 'Alice', 'Rutherford');
    aClient = await bankDao.createClient(aClient);

    let account:Account = new Account(0, 340, 'Savings');
    account = await bankDao.createAccount(aClient.clientId,account);

    account.accountType = 'Retirement'
    account = await bankDao.updateAccount(account);
    expect(account.accountType).toBe('Retirement');
});

test("Delete client by id", async () => {
    let aClient:Client = new Client(0,'Brooke', 'Beal');
    aClient = await bankDao.createClient(aClient);

    let account:Account = new Account(0, 55010, 'Savings');
    account = await bankDao.createAccount(aClient.clientId,account);

    const result:boolean = await bankDao.deleteClientById(account.accountId);
    expect(result).toBeTruthy();
})



afterAll(async() =>{
    client.end();// should close our connection once our tests are over
})